package com.calculadora.modelo;

public class Modelo{

    private int numero_1;
    private int numero_2;
    private int resultado;
    
    //Método constructor

    public int getNumero_1() {
        return numero_1;
    }
    public void setNumero_1(int numero_1) {
        this.numero_1 = numero_1;
    }
    public int getNumero_2() {
        return numero_2;
    }
    public void setNumero_2(int numero_2) {
        this.numero_2 = numero_2;
    }
    public int getResultado() {
        return resultado;
    }
    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public void sumar(){
        this.resultado = this.numero_1 + numero_2;
    }

}