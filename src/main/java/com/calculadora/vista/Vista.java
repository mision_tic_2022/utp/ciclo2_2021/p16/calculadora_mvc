package com.calculadora.vista;

import javax.swing.*;

public class Vista extends JFrame{
    /*************
     * Atributos
     ************/
    private JTextField campo_1;
    private JTextField campo_2;
    private JLabel lblOperacion;
    private JButton btnSumar;
    private JLabel lblResultado;

    /**************
     * Constructor
     **************/
    public Vista(){
        /******************************
         * Crear elementos de la ui
         *******************************/
        this.campo_1 = new JTextField(12);
        this.campo_2 = new JTextField(12);
        this.btnSumar = new JButton(" = ");
        this.lblResultado = new JLabel("");
        this.lblOperacion = new JLabel("+");
        //Creación de un objeto Panel
        JPanel panel = new JPanel();
        //Añadir los elementos al panel
        panel.add(campo_1);
        panel.add(lblOperacion);
        panel.add(campo_2);
        panel.add(btnSumar);
        panel.add(lblResultado);
        //Configurar el JFrame
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(600, 100);
        this.add(panel);
    }

    public int getCampo_1(){
        return Integer.parseInt( this.campo_1.getText() );
    }

    public int getCampo_2(){
        return Integer.parseInt( this.campo_2.getText() );
    }

    public JButton getBtnSumar(){
        return this.btnSumar;
    }

    public void setLblResultado(int resultado){
        this.lblResultado.setText( Integer.toString(resultado) );
    }

    public void mostrarVentanaError(String mensaje){
        JOptionPane.showMessageDialog(this, mensaje);
    }

}
