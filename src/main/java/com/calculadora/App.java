package com.calculadora;

import com.calculadora.controlador.Controlador;
import com.calculadora.modelo.Modelo;
import com.calculadora.vista.Vista;


public class App 
{
    public static void main( String[] args )
    {
        Vista objVista = new Vista();
        Modelo objModelo = new Modelo();

        Controlador controlador = new Controlador(objVista, objModelo);
        controlador.iniciarUI();
    }
}
