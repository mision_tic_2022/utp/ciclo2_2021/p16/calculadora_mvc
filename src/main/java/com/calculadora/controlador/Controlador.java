package com.calculadora.controlador;

import java.awt.event.*;

import com.calculadora.modelo.Modelo;
import com.calculadora.vista.Vista;

public class Controlador implements ActionListener {

    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        this.vista.getBtnSumar().addActionListener(this);
    }

    public void iniciarUI() {
        this.vista.setTitle("Calculadora");
        this.vista.setLocationRelativeTo(null);
        this.vista.setVisible(true);
    }

    public void actionPerformed(ActionEvent evt) {
        try {
            this.modelo.setNumero_1(this.vista.getCampo_1());
            this.modelo.setNumero_2(this.vista.getCampo_2());
            this.modelo.sumar();
            this.vista.setLblResultado(this.modelo.getResultado());
        } catch (Exception e) {
            // TODO: handle exception
            this.vista.mostrarVentanaError("Debe de ingresar números enteros");
        }

    }

}
